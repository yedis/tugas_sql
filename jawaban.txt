Soal 1 Membuat Database
Buatlah database dengan nama “myshop”. Tulislah di text jawaban pada nomor 1. 
create database myshop;

Soal 2 Membuat Table di Dalam Database
Buatlah tabel – tabel baru di dalam database myshop sesuai data-data berikut.

1. Tabel Users
create table users(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

2. Tabel Categories
create table categories(
    -> id int(8) primary key auto_increment,
    -> name varchar(255)
    -> );

3. Tabel Items
create table items(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(100),
    -> stock int(10),
    -> categories_id int(8),
    -> foreign key(categories_id) references categories(id)
    -> );

Soal 3 Memasukkan Data pada Table

1. Tabel Users
insert into users(name,email,password) values("John Doe","john@doe.com","john123"),("Jane   Doe","jane@doe.com","jenita123");

2. Tabel Categories
insert into categories(name) values ("gadget"),("cloth"),("men"),("women"),("branded");

2. Tabel Items
insert into items(name,description,price,stock,categories_id) values("Samsung b50","hape keren dari merek sumsang",4000000,100,1),("Uniklooh","baju keren brand ternama",500000,50,1),("IMHO Watch","jam tangan yang jujur banget",2000000,10,1);

Soal 4 Mengambil Data dari Database
a. Mengambil data users
 select id, name, email from users;

b. Mengambil data items

select * from items where price>1000000;

select * from items where name like '%watch%';

c. Menampilkan data items join dengan kategori
select items.id, items.name, items.description, items.price, items.stock, items.categories_id, categories.name from items inner join categories on items.categories_id = categories.id;

Soal 5 Mengubah Data dari Database
update items SET
    -> price =2500000 WHERE id ='1';



